sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
    "traslado/model/models",
    "jquery.sap.global",
	"sap/ui/model/json/JSONModel",
	"sap/f/FlexibleColumnLayoutSemanticHelper"
], function (UIComponent, Device, models,jQuery, JSONModel, FlexibleColumnLayoutSemanticHelper) {
	"use strict";

	
	var Component = UIComponent.extend("traslado.Component", {
		metadata: {
			manifest: "json"
		},

		init: function () {
			UIComponent.prototype.init.apply(this, arguments);

			var oModel = new JSONModel();
			this.setModel(oModel);
            
            // set products demo model on this sample
            //var oModel = new JSONModel("controller/Clothing.json");
            var oProductsModel = new JSONModel("controller/products.json");
            oProductsModel.setSizeLimit(1000);
			this.setModel(oProductsModel, "products");


			this.getRouter().initialize();
		},

		createContent: function () {
            
			return sap.ui.view({
				viewName: "traslado.view.FlexibleColumnLayout",
				type: "XML"
			});
		},

		/**
		 * Returns an instance of the semantic helper
		 * @returns {sap.f.FlexibleColumnLayoutSemanticHelper} An instance of the semantic helper
		 */
		getHelper: function () {
            
			var oFCL = this.getRootControl().byId("fcl"),
				oParams = jQuery.sap.getUriParameters(),
				oSettings = {
					defaultTwoColumnLayoutType: sap.f.LayoutType.TwoColumnsMidExpanded,
					defaultThreeColumnLayoutType: sap.f.LayoutType.ThreeColumnsMidExpanded,
					mode: oParams.get("mode"),
					initialColumnsCount: oParams.get("initial"),
					maxColumnsCount: oParams.get("max")
				};

			return FlexibleColumnLayoutSemanticHelper.getInstanceFor(oFCL, oSettings);
		}
	});
	return Component;
}, true);