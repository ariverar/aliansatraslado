sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	'sap/ui/model/Sorter',
    'sap/m/MessageBox',
    'sap/m/Label'
], function (JSONModel, Controller, Filter, FilterOperator, Sorter, MessageBox,Label) {
	"use strict";

	return Controller.extend("traslado.controller.Master", {
		onInit: function () {
			this.oRouter = this.getOwnerComponent().getRouter();
            this._bDescendingSort = false;
            this.aKeys = [
				"Category",
			"MainCategory",
			"TaxTarifCode",
			"SupplierName",
			"WeightMeasure",
			"WeightUnit",
			"Description",
			"Name",
			"DateOfSale",
			"ProductPicUrl",
			"Status",
			"Quantity",
			"UoM",
			"CurrencyCode",
			"Price",
			"Width",
			"Depth",
			"Height",
			"DimUnit"
            ];
            this.oModel = this.getOwnerComponent().getModel();
           
		},
		
		onSearch: function (oEvent) {
			var oTableSearchState = [],
				sQuery = oEvent.getParameter("query");

			if (sQuery && sQuery.length > 0) {
				oTableSearchState = [new Filter("Name", FilterOperator.Contains, sQuery)];
			}

			this.getView().byId("productsTable").getBinding("items").filter(oTableSearchState, "Application");
        },
        handlePress : function(oEvent) {
           // debugger
                var oNextUIState = this.getOwnerComponent().getHelper().getNextUIState(1);
                
                //var productPath = oEvent.getSource().getBindingContext("products").getPath();
                var productPath  = "products>/ProductCollection";
				var product = productPath.split("/").slice(-1).pop();

            this.oRouter.navTo("detail", {layout: oNextUIState.layout, product: product});
            //this.getOwnerComponent().getRouter().navTo("detail");
		},


		onAdd: function (oEvent) {
			MessageBox.show("This functionality is not ready yet.", {
				icon: MessageBox.Icon.INFORMATION,
				title: "Aw, Snap!",
				actions: [MessageBox.Action.OK]
			});
		},

		onSort: function (oEvent) {
			this._bDescendingSort = !this._bDescendingSort;
			var oView = this.getView(),
				oTable = oView.byId("productsTable"),
				oBinding = oTable.getBinding("items"),
				oSorter = new Sorter("Name", this._bDescendingSort);

			oBinding.sort(oSorter);
		},

		cambioNroAlmacen: function (oEvent) {
			//var aCurrentFilterValues = [];
            var oView = this.getView();
            //aCurrentFilterValues.push(oView.byId("nroAlm").getValue());
			//aCurrentFilterValues.push(oView.byId("tipoAlm").getValue());
           // aCurrentFilterValues.push(oView.byId("ubicacionOrigen").getValue());
            
          
            	// add filter for search
			var aFilters = [];
			var sQuery = oView.byId("nroAlm").getValue();
			if (sQuery && sQuery.length > 0) {
				var filter = new Filter("ProductId", sap.ui.model.FilterOperator.Contains, sQuery);
				aFilters.push(filter);
			}

			// update list binding
			var list = this.byId("ProductList");
            var binding = list.getBinding("items");
            
			binding.filter(aFilters);
            
		}

		
	});
}, true);